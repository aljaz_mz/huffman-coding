import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class huffmanCoding {
    public static char[] alphabet = {'a', 'b', 'c', 'd', 'e', 'f'};
    public static  float[] probabilities = {0.05f, 0.1f, 0.15f, 0.18f, 0.22f, 0.3f};
    public static String[] huffCode = {"0000", "0001", "001", "10", "11", "01"};
    public static String[] code = {"000", "001", "010", "011", "100", "101"};
    public static String input = "";
    public static String fileName = "";
    public static String fileNameChangedProbabilities = "inputChangedProb.txt";
    public static int[] intervalsForChangedProbabilities = {0, 30, 40, 55, 73, 95, 100};

    public static String encode(String input) {
        String codeWord = "";
        for (int i = 0; i < input.length(); i++) {
            for (int j = 0; j < alphabet.length; j++) {
                if (input.charAt(i) == alphabet[j]) {
                    codeWord += code[j];
                    break;
                }
            }
        }
        return codeWord;
    }

    public static String huffEncode(String input) {
        String codeWord ="";
        for (int i = 0; i < input.length(); i++) {
            for (int j = 0; j < alphabet.length; j++) {
                if (input.charAt(i) == alphabet[j]) {
                    codeWord+= huffCode[j];
                    break;
                }
            }
        }
        return codeWord;
    }

    public static String huffDecode(String input) {
        String codeWord = "";
        String inputWord = input;
        for (int i = 0; i < input.length(); i++) {
            for (int j = 0; j < huffCode.length; j++) {
                if (inputWord.startsWith(huffCode[j])) {
                    codeWord += alphabet[j];
                    inputWord = inputWord.substring(huffCode[j].length());
                    break;
                }
            }
        }
        return codeWord;
    }

    public static void readFile(String fileName) {
        try {
            java.io.File file = new java.io.File(fileName);
            Scanner scanner = new Scanner(file);
            input = "";
            while (scanner.hasNext()) {
                input += scanner.next();
            }
            scanner.close();
        } catch (java.io.FileNotFoundException e) {
            System.out.println("File not found");
        }
    }

    public static void makeFile(int length) {
        Random rand = new Random();

        String input = "";

        int r;
        for (int i = 0; i < length; i++) {

            r = rand.nextInt(100);
            for (int j = 1; j < intervalsForChangedProbabilities.length; j++) {
                if ((intervalsForChangedProbabilities[j-1] <= r) && (r < intervalsForChangedProbabilities[j])) {
                    input += alphabet[j-1];
                    break;
                }
            }
        }

        try {
            FileWriter myWriter = new FileWriter("src/inputChangedProb.txt");
            myWriter.write(input);
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static float entropy(float[] arr) {
        float sum = 0f;
        for(int i = 0; i < arr.length; i++) {
            sum += arr[i]*(Math.log(1f/arr[i])/Math.log(2));
        }
        return sum;
    }

    public static float expectedLength(String[] code, float[] arr) {
        float sum = 0f;
        for(int i = 0; i < arr.length; i++) {
            sum += (code[i].length()*arr[i]);
        }
        return sum;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter file name:");
        fileName = scanner.nextLine();

        // we make a file with different probability distribution
        makeFile(600);

        System.out.println("Original probabilities: ");
        readFile("src/"+fileName);

        System.out.println("Input: " + input + " Length: " + input.length());
        System.out.println("Encoded: " + encode(input) + " Length: " + encode(input).length());
        System.out.println("Huffman encoded: " + huffEncode(input) + "Length: " + huffEncode(input).length());
        System.out.println("Decoded: " + huffDecode(huffEncode(input)) + " Length: " + huffDecode(huffEncode(input)).length());
        System.out.println("Compression ratio: " + (float) huffEncode(input).length() / encode(input).length());

        System.out.println("--------------------------------------------------------------------------------------------------------------");

        System.out.println("Changed probabilities: ");
        readFile("src/"+fileNameChangedProbabilities);

        System.out.println("Input: " + input + " Length: " + input.length());
        System.out.println("Encoded: " + encode(input) + " Length: " + encode(input).length());
        System.out.println("Huffman encoded: " + huffEncode(input) + "Length: " + huffEncode(input).length());
        System.out.println("Decoded: " + huffDecode(huffEncode(input)) + " Length: " + huffDecode(huffEncode(input)).length());
        System.out.println("Compression ratio: " + (float) huffEncode(input).length() / encode(input).length());

        System.out.println("--------------------------------------------------------------------------------------------------------------");

        System.out.println("Entropy: " + entropy(probabilities));
        System.out.println("Expected length: " + expectedLength(huffCode, probabilities));

    }
}